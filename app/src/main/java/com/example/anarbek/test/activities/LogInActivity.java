package com.example.anarbek.test.activities;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.R;
import com.example.anarbek.test.utils.API.LoginApi;

public class LogInActivity extends Activity implements  View.OnClickListener {
    Button log_in;
    EditText sign_in, pass;
    CheckBox saveLogPass;

    SharedPreferences sPref;
    public TextView error;
    public RequestQueue requestQueue;
    public ProgressBar preloader;
    final String saveLogin = "login";
    final String savePassword = "password";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        requestQueue = Volley.newRequestQueue(this);
        preloader = findViewById(R.id.login_preloader);
        log_in = (Button) findViewById(R.id.button2);
        saveLogPass = findViewById(R.id.save_logPass_checkbox);
        log_in.setOnClickListener((View.OnClickListener) this);
        error = findViewById(R.id.login_error);
        sign_in = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.pass);




        loadText();

    }




    private void saveText() {
        sPref = getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();

        ed.putString(saveLogin, sign_in.getText().toString());
        ed.putString(savePassword, pass.getText().toString());
        ed.commit();
        Toast.makeText(LogInActivity.this, "Login and password saved", Toast.LENGTH_SHORT).show();


    }


    private void loadText() {
        sPref = getSharedPreferences("MyPref", MODE_PRIVATE);
        String savedLogin = sPref.getString(saveLogin, "");
        String savedPassword = sPref.getString(savePassword, "");
        sign_in.setText(savedLogin);
        pass.setText(savedPassword);
        Toast.makeText(LogInActivity.this, "Text loaded", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveText();
    }


    @Override
    public void onClick(View v) {
        if (saveLogPass.isChecked()) {
            saveText();
            preloader.setVisibility(View.VISIBLE);
            System.out.println(sign_in.getText().toString() + " " + pass.getText().toString());
            LoginApi.login(sign_in.getText().toString(), pass.getText().toString(), this);
        }
        else{
            preloader.setVisibility(View.VISIBLE);
            System.out.println(sign_in.getText().toString() + " " + pass.getText().toString());
            LoginApi.login(sign_in.getText().toString(), pass.getText().toString(), this);
        }


    }
}

   /* private void  login()//Функция вызываемая при нажатии кнопки
    {
        Log.v("Edittext",sign_in.getText().toString()+" "+pass.getText().toString());

    }*/

