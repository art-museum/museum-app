package com.example.anarbek.test.data;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.anarbek.test.R;

public class MainPagesAdapter extends FragmentPagerAdapter {

    private static final int PAGE_EVENTS = 0;
    private static final int PAGE_ARTOBJECTS = 1;

    private String[] titles;
    private Context context;
    public MainPagesAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        titles = context.getResources().getStringArray(R.array.tab_title);
    }

    @Override
    public Fragment getItem(int position) {
        Log.i("MainPagesAdapter", "getItem position = " + position);

        switch (position) {
            case PAGE_EVENTS:
                return ItemsFragment.createItemsFragment(ItemsFragment.TYPE_EVENTS, this.context);


            case PAGE_ARTOBJECTS:
                return ItemsFragment.createItemsFragment(ItemsFragment.TYPE_ARTOBJECTS, this.context);

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
