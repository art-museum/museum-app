package com.example.anarbek.test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.R;
import com.example.anarbek.test.model.ArtObject;
import com.example.anarbek.test.utils.API.ArtObject.ArtObjectDetail;
import com.example.anarbek.test.utils.Connectivity;
import com.example.anarbek.test.utils.PicassoImage;

import androidx.appcompat.app.AppCompatActivity;

public class DetailArtObjectActivity extends AppCompatActivity {
    public ArtObject artObject;
    public RequestQueue requestQueue;
    public TextView title, author_first_name,author_last_name, author_biography, category, description, material, period, size, created_at;
    public ImageView photo, author_photo;
    public boolean request_finished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Connectivity.isConnected(this)){
            Toast.makeText(this, "Не удалось соединится с сервером. Проверьте ваше соединение с интернетом", Toast.LENGTH_LONG).show();
            finish();
        }
        setContentView(R.layout.activity_detail_artobject);
        connectViews();
        Intent i = this.getIntent();
        Bundle b = i.getExtras();
        if (b!=null)
        artObject = new ArtObject(b.getString("primary_key", "nothing"));
        requestQueue = Volley.newRequestQueue(this);
        getSingleArtObject(artObject.getPk());
    }

    public void connectViews(){
        this.title = findViewById(R.id.artobject_title);
        this.author_first_name = findViewById(R.id.artobject_author_first_name);
        this.author_last_name = findViewById(R.id.artobject_author_last_name);
        this.author_biography = findViewById(R.id.artobject_author_biography);
        this.author_photo = findViewById(R.id.artobject_author_photo);
        this.category = findViewById(R.id.artobject_category);
        this.description = findViewById(R.id.artobject_description);
        this.material = findViewById(R.id.artobject_material);
        this.size = findViewById(R.id.artobject_size);
        this.period = findViewById(R.id.artobject_period);
        this.created_at = findViewById(R.id.artobject_created_at);
        this.photo = findViewById(R.id.artobject_photo);
    }

    public void getSingleArtObject(String primary_key){
        ArtObjectDetail.detail(primary_key, this);
    }

    public void drawObject(){
        PicassoImage.loadImage(this, artObject.getImageUrl(), this.photo);
        PicassoImage.loadImage(this, artObject.getAuthor().getPhoto(), this.author_photo);
        this.title.setText(artObject.getTitle());
        this.author_first_name.setText(artObject.getAuthor().getFirst_name());
        this.author_last_name.setText(artObject.getAuthor().getLast_name());
        this.author_biography.setText(artObject.getAuthor().getBiography());
        this.category.setText(artObject.getCategory());
        this.created_at.setText(artObject.getCreated_at());
        this.period.setText(artObject.getPeriod());
        this.size.setText(artObject.getSize());
        this.material.setText(artObject.getMaterial());
        this.description.setText(artObject.getDescription());
    }
}
