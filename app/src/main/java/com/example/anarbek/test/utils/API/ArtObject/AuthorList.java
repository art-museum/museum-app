package com.example.anarbek.test.utils.API.ArtObject;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.R;
import com.example.anarbek.test.activities.ArtObCrudActivity;
import com.example.anarbek.test.data.ItemsAdapter;
import com.example.anarbek.test.model.ArtObject;
import com.example.anarbek.test.model.Author;
import com.example.anarbek.test.model.Event;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AuthorList {
    private static String URL = Authorization.SERVER_URL+"authors/";
    private static int Method = Request.Method.GET;
    public static void list(ArtObCrudActivity activity){
        JsonArrayRequest request = new JsonArrayRequest(Method, URL,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int index =0;index < response.length(); index++){

                        JSONObject event_response = response.getJSONObject(index);
                        int primary_key = event_response.getInt("pk");
                        String Author_name = event_response.getString("full_name");
                        activity.author_data.add(new Author(primary_key, Author_name));
                        activity.author_string_data.add(Author_name);
                    }
                    activity.author_array_adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, activity.author_string_data);
                    activity.author_array_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    activity.artObjectAuthor.setAdapter(activity.author_array_adapter);
                    if (activity.getIntent().getExtras()!=null){
                        activity.artObjectAuthor.setSelection(
                                activity.author_array_adapter.getPosition(activity.getIntent().getExtras().getString("author")));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.getToken());
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }
}
