package com.example.anarbek.test.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.TextView;

import com.example.anarbek.test.R;

public class ConfirmationDialog {
    Activity activity;
    AlertDialog dialog;


    ConfirmationDialog(Activity activity){
        this.activity = activity;
    }
    public static void  startDialog(Activity activity, String confirm_text, DialogInterface.OnClickListener positive){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Предупреждение");
        builder.setMessage(confirm_text);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton(android.R.string.yes, positive);
        builder.setNegativeButton(android.R.string.no, null);
        LayoutInflater inflater = activity.getLayoutInflater();
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
