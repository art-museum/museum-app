package com.example.anarbek.test.utils.API.ArtObject;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.anarbek.test.activities.DetailArtObjectActivity;
import com.example.anarbek.test.model.Author;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
public class ArtObjectDetail {
    private static String url = Authorization.SERVER_URL + "artobjects/";
    private static int  Method = Request.Method.GET;

    public static void detail (String primary_key, DetailArtObjectActivity activity){
        JsonObjectRequest request = new JsonObjectRequest(Method, url + primary_key +"/",
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    activity.artObject.setTitle(response.getString("name"));
                    JSONObject author_response = response.getJSONObject("author");
                    Author author = new Author();
                    author.setFirst_name(author_response.getString("first_name"));
                    author.setLast_name(author_response.getString("last_name"));
                    author.setBiography(author_response.getString("biography"));
                    author.setPhoto(author_response.getString("photo"));
                    activity.artObject.setAuthor(author);
                    activity.artObject.setCategory(response.getString("category"));
                    activity.artObject.setDescription(response.getString("description"));
                    activity.artObject.setCreated_at(response.getString("created_at"));
                    activity.artObject.setMaterial(response.getString("material"));
                    activity.artObject.setPeriod(response.getString("period"));
                    activity.artObject.setSize(response.getString("size"));
                    activity.artObject.setImageUrl(response.getString("photo"));
                    activity.drawObject();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.token);
                return headers;
            }
        };
        activity.requestQueue.add(request);
    }
}