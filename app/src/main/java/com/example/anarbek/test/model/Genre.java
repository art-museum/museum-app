package com.example.anarbek.test.model;

public class Genre {
    private int primary_key;
    private String name;
    public Genre(int primary_key, String name){
        this.primary_key = primary_key;
        this.name = name;
    }

    public int getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(int primary_key) {
        this.primary_key = primary_key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
