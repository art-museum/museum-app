package com.example.anarbek.test.utils.API.Event;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.activities.AddEventActivity;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EventUpdate {
    private static String URL = Authorization.SERVER_URL+"events/";
    private static int Method = Request.Method.PUT;
    public static void update(AddEventActivity activity){
        JSONObject data = new JSONObject();
        try {
            data.put("name", activity.event_name.getText().toString());
            data.put("description", activity.event_description.getText().toString());
            data.put("occurs_at", activity.event_date.getText().toString());
            data.put("photo", activity.getStringImage());
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Method, URL + activity.primary_key + "/",
                data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                activity.preloader.setVisibility(View.GONE);
                Toast.makeText(activity, "Событие успешно изменено", Toast.LENGTH_SHORT).show();
                activity.finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.preloader.setVisibility(View.GONE);
                error.printStackTrace();
                Log.d("Error is", new String(error.networkResponse.data));
            }
        }) {
            @Override
            public Map<String, String> getHeaders () throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.getToken());
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }
}
