package com.example.anarbek.test.utils.API;

import com.android.volley.AuthFailureError;

import java.util.HashMap;
import java.util.Map;

public class Authorization {
    public static String token = "959ab437c184ca06483569882e9bb0678270ea3d";
    public static boolean is_authenticated = false;
    public static String SERVER_URL = "https://kg-museum-api.herokuapp.com/";

    public static void setToken(String token) {
        Authorization.token = token;
        is_authenticated = true;
    }

    public static String getToken() {
        return token;
    }
    public static void removeToken(){
        token = "959ab437c184ca06483569882e9bb0678270ea3d";
        is_authenticated = false;
    }
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=UTF-8");
        headers.put("Authorization", "Token " + Authorization.token);
        return headers;
    }

}
