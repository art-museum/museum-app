package com.example.anarbek.test.model;

public class ArtObject {
    private String pk;
    private String title;
    private String description;
    private String material;
    private String size;
    private String imageUrl;
    private String created_at;
    private String period;
    private String category;
    private Author author;
    private String author_full_name;
    private int primary_key;

    public ArtObject(String pk){
        this.pk = pk;
    }
    public ArtObject(){}

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }


    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getAuthor_full_name() {
        return author_full_name;
    }

    public void setAuthor_full_name(String author_full_name) {
        this.author_full_name = author_full_name;
    }

    public int getPrimary_key() {

        return primary_key;
    }

    public void setPrimary_key(int primary_key) {
        this.primary_key = primary_key;
    }
}


