package com.example.anarbek.test.utils.API.ArtObject;

import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.data.ItemsAdapter;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ArtObjectDelete {
    private static String URL = Authorization.SERVER_URL+"artobjects/";
    private static int Method = Request.Method.DELETE;
    public static void delete(ItemsAdapter adapter, String primary_key){
        JsonObjectRequest request = new JsonObjectRequest(Method, URL + primary_key + "/",
                null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(JSONObject response) {
                adapter.getArtobject_data().removeIf(x-> x.getPk().equals(primary_key));
                adapter.notifyDataSetChanged();
                Toast.makeText(adapter.context, "Экспонат успешно удален", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.getToken());
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(adapter.context);
        requestQueue.add(request);
    }
}
