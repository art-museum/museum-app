package com.example.anarbek.test.utils.API.Event;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.data.ItemsAdapter;
import com.example.anarbek.test.model.Event;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EventList {
    private static String URL = Authorization.SERVER_URL+"events/";
    private static int Method = Request.Method.GET;
    public static void list(ItemsAdapter adapter){
        JsonArrayRequest request = new JsonArrayRequest(Method, URL,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int index =0;index < response.length(); index++){

                        JSONObject event_response = response.getJSONObject(index);
                        Event event = new Event();
                        event.setName(event_response.getString("name"));
                        event.setPrimary_key(event_response.getInt("pk"));
                        event.setDescription(event_response.getString("description"));
                        event.setPhoto(event_response.getString("photo"));
                        event.setOccurs_at(event_response.getString("occurs_at"));
                        adapter.getEvent_data().add(event);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.getToken());
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(adapter.context);
        requestQueue.add(request);
    }
}
