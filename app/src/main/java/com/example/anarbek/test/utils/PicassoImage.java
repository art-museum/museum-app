package com.example.anarbek.test.utils;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.example.anarbek.test.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class PicassoImage {
    public static void loadImage(Context context, String imageUrl, ImageView to_image){
        if (Connectivity.isConnected(context)) {
            Picasso.get().load(imageUrl).placeholder(R.drawable.image_preloader).error(R.drawable.not_found_image).into(to_image, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d("Loaded", "hello");
                }

                @Override
                public void onError(Exception e) {
                    Log.d("Image Internet Error", e.toString());
                    Picasso.get().load(imageUrl).placeholder(R.drawable.image_preloader).error(R.drawable.not_found_image).networkPolicy(NetworkPolicy.OFFLINE).into(to_image, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d("Loaded", "hello");
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.d("Image Cache Error", e.toString());
                        }
                    });
                }
            });
        }
        else {
            Picasso.get().load(imageUrl).placeholder(R.drawable.image_preloader).error(R.drawable.not_found_image).networkPolicy(NetworkPolicy.OFFLINE).into(to_image, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d("Loaded", "hello");
                }

                @Override
                public void onError(Exception e) {
                    Log.d("Image Cache Error", e.toString());
                }
            });
        }
    }
}
