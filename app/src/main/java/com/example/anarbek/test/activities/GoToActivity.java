package com.example.anarbek.test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.anarbek.test.R;
import com.example.anarbek.test.data.MainPagesAdapter;
import com.example.anarbek.test.utils.API.Authorization;
import com.example.anarbek.test.utils.ReloadActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.viewpager.widget.ViewPager;

public class GoToActivity extends AppCompatActivity implements OnClickListener,PopupMenu.OnMenuItemClickListener {
    FloatingActionButton scannBtn;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private MainPagesAdapter adapter;
    public RequestQueue requestQueue;
    Button del;
    Button chan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_to);

        scannBtn = (FloatingActionButton) findViewById(R.id.scanBtn);
        scannBtn.setOnClickListener(this);


        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new MainPagesAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scanBtn:
                Intent intent = new Intent(this, CameraActivity.class);
                startActivityForResult(intent, 1);
                break;
            default:
                break;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data == null) {
                return;
            }
            if (Objects.equals(data.getStringExtra("error"), "server_error")) {
                Toast.makeText(GoToActivity.this, "Не удалось соединится с сервером. Проверьте ваше соединение с сетью", Toast.LENGTH_LONG).show();
            }
        }
        else if(requestCode == 2){
            adapter.notifyDataSetChanged();
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart");
    }


    private void checkAuthenticated(Menu popupMenu){
        if (Authorization.is_authenticated){
            popupMenu.findItem(R.id.log_in).setVisible(false);
            popupMenu.findItem(R.id.log_out).setVisible(true);
            popupMenu.findItem(R.id.add_art).setVisible(true);
            popupMenu.findItem(R.id.add_event).setVisible(true);
        }


        else{
            popupMenu.findItem(R.id.log_in).setVisible(true);
            popupMenu.findItem(R.id.log_out).setVisible(false);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.main);
        popup.show();
        checkAuthenticated(popup.getMenu());
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_in:
                Intent intent = new Intent(this, LogInActivity.class);
                startActivityForResult(intent, 1);
                return true;
            case R.id.log_out:
                Toast.makeText(this.getBaseContext(), "Вы успешно вышли из системы", Toast.LENGTH_SHORT).show();
                Authorization.removeToken();
                ReloadActivity.reload(this);
                return true;

            case R.id.add_event:
                Intent intent4 = new Intent(this, AddEventActivity.class);
                startActivityForResult(intent4, 2);

                return true;
            case R.id.add_art:
                Intent intent5 = new Intent(this, ArtObCrudActivity.class);
                startActivityForResult(intent5, 1);
                return true;
            default:
                return false;
        }
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu., menu);
//        return true;
//    }

}