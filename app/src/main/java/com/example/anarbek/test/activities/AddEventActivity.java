package com.example.anarbek.test.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.anarbek.test.R;
import com.example.anarbek.test.utils.API.Event.EventCreate;
import com.example.anarbek.test.utils.API.Event.EventUpdate;
import com.example.anarbek.test.utils.PicassoImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class AddEventActivity extends AppCompatActivity {
    int GALLERY_REQUEST=123;
    public Bitmap bitmap;

    public ImageView event_img;
    Button event_add_btn, event_img_btn,save_event_button;
    public int primary_key;
    public ProgressBar preloader;

    public EditText event_date,event_name, event_description;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        event_img =findViewById(R.id.eventObImage_imgview);
        event_name = findViewById(R.id.event_name_edtxt);
        event_date = findViewById(R.id.event_date_edtxt);
        event_description = findViewById(R.id.event_text_edtxt);
        event_img_btn =findViewById(R.id.event_img_addBtn);
        event_add_btn = findViewById(R.id.add_event_button);
        save_event_button=findViewById(R.id.save_event_button);
        preloader = findViewById(R.id.add_event_preloader);
        if (getIntent().getExtras()!=null) {
            loadData(getIntent().getExtras());
        }
        event_add_btn.setEnabled(false);
        event_add_btn.setVisibility(View.INVISIBLE);
        save_event_button.setKeyListener(null);


        event_date.addTextChangedListener(textValidation);
        event_name.addTextChangedListener(textValidation);
        event_description.addTextChangedListener(textValidation);



        event_date.setInputType(InputType.TYPE_NULL);
        event_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimeDialog(event_date);
            }
        });

        event_img_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Выберите картинку"), GALLERY_REQUEST);

            }
        });
        save_event_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameInput = event_name.getText().toString().trim();
                String dateInput = event_date.getText().toString().trim();
                String descrInput= event_description.getText().toString().trim();
                if(!nameInput.isEmpty() && !dateInput.isEmpty() && !descrInput.isEmpty() &&
                        (event_img.getDrawable()!=null))
                {
                    event_add_btn.setEnabled(true);
                    event_add_btn.setVisibility(View.VISIBLE);

                }
                else{
                    showToast(v);
                    event_add_btn.setEnabled(false);
                    event_add_btn.setVisibility(View.INVISIBLE);

                }

            }
        });


        event_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preloader.setVisibility(View.VISIBLE);
                if (getIntent().getExtras()==null) {
                    EventCreate.create(AddEventActivity.this);
                }
                else{
                    EventUpdate.update(AddEventActivity.this);

                }
            }
        });

    }
    public void showToast(View view) {
        //создаём и отображаем текстовое уведомление
        Toast toast = Toast.makeText(getApplicationContext(),
                "Заполните всю форму!",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    private TextWatcher textValidation = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String nameInput = event_name.getText().toString().trim();
            String dateInput = event_date.getText().toString().trim();
            String descrInput= event_description.getText().toString().trim();
            event_add_btn.setEnabled(!nameInput.isEmpty() && !dateInput.isEmpty() && !dateInput.isEmpty() &&
        (event_img.getDrawable()!=null));
            if(nameInput.isEmpty() || descrInput.isEmpty() || dateInput.isEmpty() ||
                    (event_img.getDrawable()!=null))
                event_add_btn.setVisibility(View.INVISIBLE);


        }



        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private void loadData(Bundle data) {
        event_name.setText(data.getString("name"));
        event_description.setText(data.getString("description"));
        event_date.setText(data.getString("occurs_at"));
        event_img_btn.setText("Изменить Картинку");
        PicassoImage.loadImage(this, data.getString("photo"), event_img);
        primary_key = data.getInt("pk");
        event_add_btn.setText("Изменить");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri imageData = data.getData();
            try {
                if (Build.VERSION.SDK_INT < 28) {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageData);
                    event_img.setImageBitmap(bitmap);
                }
                else{
                    ImageDecoder.Source source = ImageDecoder.createSource(getContentResolver(), imageData);
                    bitmap = ImageDecoder.decodeBitmap(source);
                    event_img.setImageBitmap(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public String getStringImage(){
        if (bitmap==null){
            bitmap = ((BitmapDrawable)event_img.getDrawable()).getBitmap();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    private void showDateTimeDialog(final EditText date_time_in) {
        final Calendar calendar=Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        calendar.set(Calendar.SECOND, 0);
                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                        date_time_in.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                };

                new TimePickerDialog(AddEventActivity.this,timeSetListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),false).show();
            }
        };

        new DatePickerDialog(AddEventActivity.this,dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();

    }
}
