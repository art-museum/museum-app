package com.example.anarbek.test.model;

public class Event {
    private int primary_key;
    private String name;
    private String occurs_at;
    private String description;
    private String photo;
    public Event(){}
    public Event(String name, String occurs_at){
        this.name = name;
        this.occurs_at = occurs_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccurs_at() {
        return occurs_at;
    }

    public void setOccurs_at(String occurs_at) {
        this.occurs_at = occurs_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(int primary_key) {
        this.primary_key = primary_key;
    }
}
