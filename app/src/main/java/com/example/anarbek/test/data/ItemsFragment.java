package com.example.anarbek.test.data;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.anarbek.test.R;
import com.example.anarbek.test.activities.DetailArtObjectActivity;
import com.example.anarbek.test.model.ArtObject;
import com.example.anarbek.test.model.Event;
import com.example.anarbek.test.utils.Connectivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class ItemsFragment<view, refresh> extends Fragment {
    SwipeRefreshLayout refresh;
    public static final int TYPE_EVENTS = 0;
    public static final int TYPE_ARTOBJECTS = 1;
    public int type;
    private static final String TYPE_KEY = "type";
    SharedPreferences shref;
    SharedPreferences.Editor editor;

    public ItemsFragment(int type) {
        super();
        this.type = type;
    }

    @Override
    public void onStop() {
        super.onStop();
        shref = getContext().getSharedPreferences("Data", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json, json_initial;
        editor = shref.edit();
        if (this.type == 1 && adapter.getArtobject_data().size() != 0) {
            List<ArtObject> artObject = adapter.getArtobject_data();
            json_initial = gson.toJson(artObject.subList(0, 10));
            editor.remove("initialArtObject").apply();
            editor.putString("initialArtObject", json_initial).commit();
            json = gson.toJson(artObject);
            editor.remove("artObject").apply();
            editor.putString("artObject", json);
        } else if (this.type == 0 && adapter.getEvent_data().size() != 0) {
            json = gson.toJson(adapter.getEvent_data());
            editor.remove("event").apply();
            editor.putString("event", json);
            Log.d("EVENT", ""+adapter.getEvent_data().size());
        }
        editor.commit();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Gson gson = new Gson();
        shref = getContext().getSharedPreferences("Data", Context.MODE_PRIVATE);
        if (Connectivity.isConnected(getContext()))
            adapter.getData();
        else {
            if (this.type == 0 && adapter.getEvent_data().size() == 0) {
                String response = shref.getString("event", "");
                if (!response.equals("")) {
                    ArrayList<Event> eventData = gson.fromJson(response,
                            new TypeToken<List<Event>>() {
                            }.getType());
                    adapter.setEvent_data(eventData);
                }
            } else if (this.type == 1 && adapter.getArtobject_data().size() == 0) {
                String response = shref.getString("initialArtObject", "");
                if (!response.equals("")) {
                    ArrayList<ArtObject> artObjectData = gson.fromJson(response,
                            new TypeToken<List<ArtObject>>() {
                            }.getType());
                    adapter.setArtobject_data(artObjectData);
                    adapter.next_page++;
                }
            }
        }
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onStart() {
        super.onStart();
        editor = shref.edit();
        Gson gson = new Gson();
        if (this.type == 1 && adapter.getArtobject_data().size() == 0 && !adapter.request_send) {
            String response = shref.getString("artObject", "");
            if (!response.equals("")) {
                ArrayList<ArtObject> artObjectData = gson.fromJson(response,
                        new TypeToken<List<ArtObject>>() {
                        }.getType());
                adapter.setArtobject_data(artObjectData);
                Log.d("ArtObject", "LocalStorage");
                editor.remove("artObject").apply();
            }
        }
        if (this.type == 1 && adapter.getEvent_data().size() == 0 ) {
            String response = shref.getString("event", "");
            if (!response.equals("")) {
                ArrayList<Event> events = gson.fromJson(response,
                        new TypeToken<List<Event>>() {
                        }.getType());
                adapter.setEvent_data(events);
                editor.remove("event").apply();
            }
        }
        adapter.notifyDataSetChanged();
    }


    public static ItemsFragment createItemsFragment(int type, Context v) {

        ItemsFragment fragment = new ItemsFragment(type);

        Bundle bundle = new Bundle();
        bundle.putInt(ItemsFragment.TYPE_KEY, type);

        fragment.setArguments(bundle);
        return fragment;
    }


    private RecyclerView recycler;
    private ItemsAdapter adapter;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefresh;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        type = bundle.getInt(TYPE_KEY, 2);

        adapter = new ItemsAdapter(this.getActivity(), type);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (this.type == 0)
            view = inflater.inflate(R.layout.event_items, container, false);
        else {
            view = inflater.inflate(R.layout.artobject_items, container, false);
        }
        swipeRefresh=view.findViewById(R.id.refresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.getData();
                swipeRefresh.setRefreshing(false);
            }
        });
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        if (this.type == 0)
            recycler = view.findViewById(R.id.event_items);
        else {
            recycler = view.findViewById(R.id.artobject_items);
            progressBar = view.findViewById(R.id.progressBar);
            adapter.setClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = recycler.getChildAdapterPosition(v);
                    Intent intent = new Intent(getContext(), DetailArtObjectActivity.class);
                    intent.putExtra("primary_key", adapter.getArtobject_data().get(pos).getPk());
                    startActivityForResult(intent, 1);
                }
            });
            recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    if (!recyclerView.canScrollVertically(1) && !adapter.request_send) {
                        progressBar.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), "next page is  " + adapter.next_page, Toast.LENGTH_LONG).show();
                        adapter.getMoreData();
                    } else {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);
    }

}



