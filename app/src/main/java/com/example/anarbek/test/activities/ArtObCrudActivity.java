package com.example.anarbek.test.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anarbek.test.R;
import com.example.anarbek.test.model.Author;
import com.example.anarbek.test.model.Genre;
import com.example.anarbek.test.utils.API.ArtObject.ArtObjectCreate;
import com.example.anarbek.test.utils.API.ArtObject.ArtObjectUpdate;
import com.example.anarbek.test.utils.API.ArtObject.AuthorList;
import com.example.anarbek.test.utils.API.ArtObject.CategoryList;
import com.example.anarbek.test.utils.PicassoImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class ArtObCrudActivity extends AppCompatActivity {
    public ImageView imageView;
    public String primary_key;
    public Bitmap bitmap;
    public ProgressBar preloader;
    public TextView title, year, description, size, period, material;
    Button artObAddImgBtn, artObAddBtn,artOb_save_button;
    public Spinner artObjectAuthor, artObNameGenre;
    public List<Author> author_data = new ArrayList<Author>();
    public List<Genre> categories_data = new ArrayList<Genre>();
    public List<String> author_string_data = new ArrayList<String>(), categories_string_data = new ArrayList<String>();
    public ArrayAdapter<String> author_array_adapter;
    public ArrayAdapter<String> categories_adapter;
    int GALLERY_REQUEST=123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud_art_object);
        title = findViewById(R.id.artObName_edtxt);
        year = findViewById(R.id.artObData_edtxt);
        description = findViewById(R.id.artObNameText_edtxt);
        size = findViewById(R.id.artObSize_edtxt);
        period = findViewById(R.id.artObPeriod_edtxt);
        material = findViewById(R.id.artObMaterial_edtxt);
        imageView=findViewById(R.id.artObImage_imgview);
        artObAddImgBtn=findViewById(R.id.artOb_img_addBtn);
        artObAddBtn = findViewById(R.id.add_art_button);
        artOb_save_button= findViewById(R.id.save_art_button);
        artObjectAuthor = findViewById(R.id.artObAuthor_edtxt);
        artObNameGenre = findViewById(R.id.artObNameGenre_edtxt);
        preloader = findViewById(R.id.artObCrud_preloader);
        title.addTextChangedListener(textValidation);
        year.addTextChangedListener(textValidation);
        period.addTextChangedListener(textValidation);
        description.addTextChangedListener(textValidation);
        size.addTextChangedListener(textValidation);
        material.addTextChangedListener(textValidation);


        artObAddBtn.setEnabled(false);
        artObAddBtn.setVisibility(View.INVISIBLE);
        if (getIntent().getExtras()!=null){
            loadData(getIntent().getExtras());
        }
        CategoryList.list(this);
        AuthorList.list(this);
        artObAddImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Pick an image"), GALLERY_REQUEST);

            }
        });

        artOb_save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameInput = title.getText().toString().trim();
                String dateInput = year.getText().toString().trim();
                String descrInput= description.getText().toString().trim();

                String sizeInput= size.getText().toString().trim();
                String periodInput=period.getText().toString().trim();
                String materialInput=material.getText().toString().trim();


                if(!nameInput.isEmpty() && !dateInput.isEmpty() && !descrInput.isEmpty() &&
                        (imageView.getDrawable()!=null)  && !sizeInput.isEmpty() &&
                        !periodInput.isEmpty() && !materialInput.isEmpty() )
                {
                    artObAddBtn.setEnabled(true);
                    artObAddBtn.setVisibility(View.VISIBLE);

                }
                else{
                    showToast(v);
                    artObAddBtn.setEnabled(false);
                    artObAddBtn.setVisibility(View.INVISIBLE);

                }

            }
        });

        artObAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getExtras()!=null)
                    ArtObjectUpdate.update(ArtObCrudActivity.this);
                else
                ArtObjectCreate.create(ArtObCrudActivity.this);
            }
        });

    }

    private void loadData(Bundle data) {
        primary_key = data.getString("pk");
        title.setText(data.getString("title"));
        description.setText(data.getString("description"));
        material.setText(data.getString("material"));
        size.setText(data.getString("size"));
        period.setText(data.getString("period"));
        year.setText(data.getString("created_at"));
        PicassoImage.loadImage(this, data.getString("photo"), imageView);

    }

    public void showToast(View view) {
        //создаём и отображаем текстовое уведомление
        Toast toast = Toast.makeText(getApplicationContext(),
                "Заполните всю форму!",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    private TextWatcher textValidation = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String nameInput = title.getText().toString().trim();
            String dateInput = year.getText().toString().trim();
            String descrInput= description.getText().toString().trim();

            String sizeInput= size.getText().toString().trim();
            String periodInput=period.getText().toString().trim();
            String materialInput=material.getText().toString().trim();
            artObAddBtn.setEnabled((!nameInput.isEmpty() && !dateInput.isEmpty() && !descrInput.isEmpty() &&
                    (imageView.getDrawable()!=null)
                   && !sizeInput.isEmpty() &&
                    !periodInput.isEmpty() && !materialInput.isEmpty() ));

            if(nameInput.isEmpty() || dateInput.isEmpty() || descrInput.isEmpty() ||
                    (imageView.getDrawable()==null)  || sizeInput.isEmpty() ||
                    periodInput.isEmpty()  || materialInput.isEmpty() )
                artObAddBtn.setVisibility(View.INVISIBLE);



        }



        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri imageData = data.getData();
            try {
                if (Build.VERSION.SDK_INT < 28) {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageData);
                    imageView.setImageBitmap(bitmap);
                }
                else{
                    ImageDecoder.Source source = ImageDecoder.createSource(getContentResolver(), imageData);
                    bitmap = ImageDecoder.decodeBitmap(source);
                    imageView.setImageBitmap(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public String getStringImage(){
        if (bitmap==null){
            bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
