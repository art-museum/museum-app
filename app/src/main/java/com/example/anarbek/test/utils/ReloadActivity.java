package com.example.anarbek.test.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.example.anarbek.test.activities.GoToActivity;

public class ReloadActivity {
    public static void reload(Activity c){
        Intent i = new Intent(c, c.getClass());
        c.finish();
        c.overridePendingTransition(1, 1);
        c.startActivity(i);
        c.overridePendingTransition(1, 1);
    }
}
