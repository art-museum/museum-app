package com.example.anarbek.test.data;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anarbek.test.R;
import com.example.anarbek.test.activities.AddEventActivity;
import com.example.anarbek.test.activities.ArtObCrudActivity;
import com.example.anarbek.test.activities.GoToActivity;
import com.example.anarbek.test.model.ArtObject;
import com.example.anarbek.test.model.Event;
import com.example.anarbek.test.utils.API.ArtObject.ArtObjectDelete;
import com.example.anarbek.test.utils.API.ArtObject.ArtObjectList;
import com.example.anarbek.test.utils.API.Authorization;
import com.example.anarbek.test.utils.API.Event.EventDelete;
import com.example.anarbek.test.utils.API.Event.EventList;
import com.example.anarbek.test.utils.ConfirmationDialog;
import com.example.anarbek.test.utils.PicassoImage;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemViewHolder> {

    private List<Event> event_data = new ArrayList<>();
    private List<ArtObject> artobject_data = new ArrayList<>();
    public int next_page = 0;
    private View.OnClickListener mClickListener;
    public Context context;
    private int type;
    public boolean request_send = false;
    public ProgressBar progressBar;
    public List<ArtObject> getArtobject_data() {
        return artobject_data;
    }
    public void setArtobject_data(List <ArtObject> artobject_data){
        this.artobject_data = artobject_data;
    }

    public List<Event> getEvent_data(){
        return event_data;
    }

    public void setEvent_data(List <Event> event_data){
        this.event_data = event_data;
    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }
    public ItemsAdapter(Context context, int type) {
        this.type = type;
        this.context = context;

    }



    @Override
    public ItemsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        if (this.type == 0)
            view = inflater.inflate(R.layout.event_item, parent, false);
        else {
            view = inflater.inflate(R.layout.artobject_item, parent, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onClick(view);
                }
            });
        }
        return new ItemViewHolder(view, this.type, this);
    }

    @Override
    public void onBindViewHolder(ItemsAdapter.ItemViewHolder holder, int position) {
        if (this.type == 0) {
            holder.applyEventData(event_data.get(position));
            Log.d("event", Integer.toString(event_data.get(position).getPrimary_key()));
        }
        else {

            holder.applyArtObjectData(artobject_data.get(position));
            Log.d("event", artobject_data.get(position).getPk());
        }
    }

    @Override
    public int getItemCount() {
        if (this.type == 0)
            return event_data.size();
        else
            return artobject_data.size();
    }

    public void getData(){
            this.request_send = true;
            if (this.type == 0) {
                getEvent_data().clear();
                getEventsData();
            }
            else {
                next_page = 0;
                getArtobject_data().clear();
                getArtObjectsData();
            }


    }
    public void getMoreData(){
        this.request_send = true;
        if (this.type == 0)
            getEventsData();
        else
            getArtObjectsData();
    }

    private void getEventsData() {
        EventList.list(this);
    }

    private void getArtObjectsData() {
        ArtObjectList.list(this);
    }



    static class ItemViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout event_change_delete,artOb_change_delete_layout;
        private Button event_change, event_delete,artOb_change_button,artOb_delete_button;
        ItemsAdapter adapter;

        private  TextView event_name;
        private  TextView event_occurs_at;
        private  ImageView event_image;
        private  TextView event_description;
        View itemView;
        private TextView artObject_title;
        private TextView artObject_size;
        private TextView artObject_period;
        private TextView artObject_material;
        private TextView artObject_category;
        private TextView artObject_created_at;
        private TextView artObject_author_full_name;
        private ImageView artObject_photo;

        public ItemViewHolder(View itemView, int type, ItemsAdapter adapter) {
            super(itemView);
            if (type ==0) {
                event_change_delete = itemView.findViewById(R.id.event_change_delete);
                event_change = itemView.findViewById(R.id.event_change_button);
                event_delete = itemView.findViewById(R.id.event_delete_button);
                event_name = itemView.findViewById(R.id.event_name);
                event_occurs_at = itemView.findViewById(R.id.event_occurs_at);
                event_description = itemView.findViewById(R.id.event_description);
                event_image = itemView.findViewById(R.id.event_image);
            }
            else {
                artOb_change_delete_layout=itemView.findViewById(R.id.artOb_change_delete);
                artOb_change_button=itemView.findViewById(R.id.artOb_change_button);
                artOb_delete_button=itemView.findViewById(R.id.artOb_delete_button);
                artObject_title = itemView.findViewById(R.id.list_artobject_title);
                artObject_category = itemView.findViewById(R.id.list_artobject_category);
                artObject_material = itemView.findViewById(R.id.list_artobject_material);
                artObject_created_at = itemView.findViewById(R.id.list_artobject_created_at);
                artObject_author_full_name = itemView.findViewById(R.id.list_artobject_author);
                artObject_period = itemView.findViewById(R.id.list_artobject_period);
                artObject_photo = itemView.findViewById(R.id.list_artobject_image);
                artObject_size = itemView.findViewById(R.id.list_artobject_size);
            }
            this.itemView = itemView;
            this.adapter = adapter;
        }

        public void applyEventData(Event record) {

            event_name.setText(record.getName());
            event_occurs_at.setText(record.getOccurs_at());
            event_description.setText(record.getDescription());
            PicassoImage.loadImage(itemView.getContext(), record.getPhoto(), event_image);
            applyEventButtons(record);

        }
        public void applyEventButtons(Event record){
            if (Authorization.is_authenticated) {
                event_change_delete.setVisibility(View.VISIBLE);

                event_change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(itemView.getContext(), "Change " +
                                record.getPrimary_key(), Toast.LENGTH_SHORT).show();

                        GoToActivity activity = (GoToActivity)adapter.context;
                        Intent intent = new Intent(activity, AddEventActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("name", record.getName());
                        bundle.putInt("pk", record.getPrimary_key());
                        bundle.putString("description", record.getDescription());
                        bundle.putString("occurs_at", record.getOccurs_at());
                        bundle.putString("photo", record.getPhoto());
                        intent.putExtras(bundle);
                        activity.startActivityForResult(intent, 2);

                    }
                });
                event_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ConfirmationDialog.startDialog((Activity)itemView.getContext(),
                                "Вы уверены  что хотите удалить " + record.getName() + "?",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        EventDelete.delete(adapter, record.getPrimary_key());
                                    }});
                    }
                });
            }
            else
                event_change_delete.setVisibility(View.GONE);

        }
        public void applyArtObButtons(ArtObject record){
            if (Authorization.is_authenticated) {

                artOb_change_delete_layout.setVisibility(View.VISIBLE);
                artOb_change_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        GoToActivity activity = (GoToActivity)adapter.context;
                        Intent intent = new Intent(activity, ArtObCrudActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("title", record.getTitle());
                        bundle.putString("pk", record.getPk());
                        bundle.putString("description", record.getDescription());
                        bundle.putString("created_at", record.getCreated_at());
                        bundle.putString("photo", record.getImageUrl());
                        bundle.putString("material", record.getMaterial());
                        bundle.putString("period", record.getPeriod());
                        bundle.putString("size", record.getSize());
                        bundle.putString("category", record.getCategory());
                        bundle.putString("author", record.getAuthor_full_name());
                        Log.d("Author_FULL_NAME", record.getAuthor_full_name());
                        intent.putExtras(bundle);
                        activity.startActivityForResult(intent, 2);

                    }
                });
                artOb_delete_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ConfirmationDialog.startDialog((Activity)itemView.getContext(),
                                "Вы уверены  что хотите удалить " + record.getTitle() + "?",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        ArtObjectDelete.delete(adapter, record.getPk());
                                    }});
                    }
                });
            }
            else
                artOb_change_delete_layout.setVisibility(View.GONE);

        }

        public void applyArtObjectData(ArtObject record){
            artObject_size.setText(record.getSize());
            artObject_period.setText(record.getPeriod());
            artObject_author_full_name.setText(record.getAuthor_full_name());
            artObject_material.setText(record.getMaterial());
            artObject_title.setText(record.getTitle());
            artObject_category.setText(record.getCategory());
            artObject_created_at.setText(record.getCreated_at());
            PicassoImage.loadImage(itemView.getContext(), record.getImageUrl(), artObject_photo);
            applyArtObButtons(record);
        }

     DialogInterface.OnClickListener a = new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Toast.makeText(itemView.getContext(), "Yaay", Toast.LENGTH_SHORT).show();
                                    }};
    }
}
