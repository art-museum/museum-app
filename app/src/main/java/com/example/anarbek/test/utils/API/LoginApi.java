package com.example.anarbek.test.utils.API;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.anarbek.test.activities.LogInActivity;
import com.example.anarbek.test.utils.Connectivity;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginApi {
    private static String url = Authorization.SERVER_URL + "accounts/login/";
    private static int  Method = Request.Method.POST;

    public static void login (String username, String password,  LogInActivity activity)  {
        JSONObject jsonobject = new JSONObject();
        activity.error.setVisibility(View.INVISIBLE);
        try {
            jsonobject.put("username", username);
            jsonobject.put("password", password);
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Method, url,
                jsonobject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Key is : ", response.getString("key"));
                    activity.preloader.setVisibility(View.INVISIBLE);
                    Authorization.setToken(response.getString("key"));
                    Toast.makeText(activity, "Вы были успешно авторизированы!", Toast.LENGTH_LONG).show();
                    activity.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (Connectivity.isConnected(activity)){
                    activity.error.setText("Неверное имя пользователя или пароль.");
                }
                else{
                    activity.error.setText("Ошибка подключения к сети. Проверьте доступ в Интернет.");
                }
                activity.error.setVisibility(View.VISIBLE);
                activity.preloader.setVisibility(View.INVISIBLE);
                error.printStackTrace();
            }
        });
        activity.requestQueue.add(request);
    }
}
