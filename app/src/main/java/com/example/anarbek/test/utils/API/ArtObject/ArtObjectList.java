package com.example.anarbek.test.utils.API.ArtObject;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.data.ItemsAdapter;
import com.example.anarbek.test.data.ItemsFragment;
import com.example.anarbek.test.model.ArtObject;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ArtObjectList {
    private static  RequestQueue requestQueue;
    private static String URL;
    private static int Method = Request.Method.GET;

    public static void list(ItemsAdapter adapter){
        URL  = Authorization.SERVER_URL+"artobjects/";
        adapter.next_page++;

        if (adapter.next_page > 1)
            URL = URL + "?page=" + adapter.next_page;
        Log.d("URL: ", URL);
        JsonObjectRequest request = new JsonObjectRequest(Method, URL,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray artobject_array = response.getJSONArray("results");
                    for (int index =0;index < artobject_array.length(); index++){

                        JSONObject artObject_response = artobject_array.getJSONObject(index);
                        Log.d("Artobject", artObject_response.toString());
                        ArtObject artObject = new ArtObject();
                        artObject.setPk(artObject_response.getString("pk"));
                        artObject.setTitle(artObject_response.getString("name"));
                        artObject.setCategory(artObject_response.getString("category"));
                        artObject.setSize(artObject_response.getString("size"));
                        artObject.setMaterial(artObject_response.getString("material"));
                        artObject.setCreated_at(artObject_response.getString("created_at"));
                        artObject.setPeriod(artObject_response.getString("period"));
                        artObject.setImageUrl(artObject_response.getString("photo"));
                        artObject.setAuthor_full_name(artObject_response.getString("author"));
                        artObject.setDescription(artObject_response.getString("description"));
                        adapter.getArtobject_data().add(artObject);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.request_send = false;
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                adapter.request_send = false;
                adapter.next_page--;
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.getToken());
                return headers;
            }
        };

        requestQueue = Volley.newRequestQueue(adapter.context);
        requestQueue.add(request);
    }
}
