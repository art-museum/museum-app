package com.example.anarbek.test.utils.API.ArtObject;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.anarbek.test.activities.AddEventActivity;
import com.example.anarbek.test.activities.ArtObCrudActivity;
import com.example.anarbek.test.utils.API.Authorization;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ArtObjectCreate {
    private static String URL = Authorization.SERVER_URL+"artobjects/";
    private static int Method = Request.Method.POST;
    public static void create(ArtObCrudActivity activity){
        activity.preloader.setVisibility(View.VISIBLE);
        JSONObject data = new JSONObject();
        try {
            data.put("name", activity.title.getText().toString());
            data.put("description", activity.description.getText().toString());
            data.put("material", activity.material.getText().toString());
            data.put("size", activity.size.getText().toString());
            data.put("period", activity.period.getText().toString());
            data.put("created_at", activity.year.getText().toString());
            data.put("category", activity.categories_data.get(activity.artObNameGenre.getSelectedItemPosition()).getPrimary_key());
            data.put("author", activity.author_data.get(activity.artObjectAuthor.getSelectedItemPosition()).getPrimary_key());

            data.put("photo", activity.getStringImage());
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Method, URL,
                data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                activity.preloader.setVisibility(View.GONE);
                Toast.makeText(activity, "Новый экспонат успешно создано", Toast.LENGTH_SHORT).show();
                activity.finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.preloader.setVisibility(View.GONE);
                error.printStackTrace();
                Log.d("Error is", new String(error.networkResponse.data));
            }
        }) {
            @Override
            public Map<String, String> getHeaders () throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Token " + Authorization.getToken());
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }
}
